// this file is just a simple JUnit test(s)

import merchantservice.bankService.BankController;
import merchantservice.controller.MerchantController;
import merchantservice.database.Merchant;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messageQueue.EventSender;
import messageQueue.rabbitmq.RabbitMQAdapter;
import messageQueue.rabbitmq.RabbitMqSender;
import org.junit.*;
import merchantservice.restAdapter.DTO.UpdateMerchant;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @Author Lukas Nyboe Bek - s153475
 */
public class MerchantDAOTest {

    static MerchantController merchantController;
    Merchant merchant;
    String merchantId;
    String testerName;
    String testerCpr;
    String testerBankAccount;

    List<String> createdAccounts = new ArrayList<>();

    @BeforeClass
    public static void setUpClass() throws Exception {

        BankService bank = new BankServiceService().getBankServicePort();
        BankController bankController = new BankController(bank);
        EventSender eventSender = new RabbitMqSender();
        RabbitMQAdapter rabbitMQAdapter = new RabbitMQAdapter(eventSender);
        merchantController = new MerchantController(bankController, rabbitMQAdapter);
    }

    @Before
    public void setUp() throws Exception {
        testerName = "testerName";
        testerCpr = "testerCpr";
        testerBankAccount = "testerBankAccount";
        merchant = new Merchant(testerCpr, testerName, testerBankAccount);
        merchantId = merchantController.addMerchant(merchant);

        createdAccounts.add(merchantId);
    }

    @Test
    public void testGetMerchant() {
        // This is one of the test merchants used to populate the merchantservice.database
        Optional<Merchant> merchant = merchantController.getMerchant(merchantId);

        assertEquals(merchant.get().getBankAccountId(), testerBankAccount);
        assertEquals(merchant.get().getName(), testerName);
        assertEquals(merchant.get().getCpr(), testerCpr);
    }

    @Test
    public void testCreateMerchant() {
        Merchant createMerchant = new Merchant("createMerchantCpr", "createMerchantName", "createMerchantBankaccountId");
        String merchantId = merchantController.addMerchant(createMerchant);

        createdAccounts.add(merchantId);

        Optional<Merchant> createdMerchant = merchantController.getMerchant(merchantId);

        assertTrue(createdMerchant.isPresent());
    }

    @Test
    public void testUpdateMerchant() {
        merchantController.updateMerchant(new UpdateMerchant(merchantId, "newCpr", "newName", "newBankaccount"));

        Optional<Merchant> updateMerchant = merchantController.getMerchant(merchantId);

        assertEquals(updateMerchant.get().getCpr(), "newCpr");
        assertEquals(updateMerchant.get().getName(), "newName");
        assertEquals(updateMerchant.get().getBankAccountId(), "newBankaccount");
    }

    @Test
    public void testDeleteMerchant() {
        Merchant merchantToBeDeleted = new Merchant("deleteMerchantCpr", "deleteMerchantName", "deleteMerchantBankAccountId");
        String merchantToBeDeletedId = merchantController.addMerchant(merchantToBeDeleted);

        Optional<Merchant> deletedMerchantExist = merchantController.getMerchant(merchantToBeDeletedId);
        assertTrue(deletedMerchantExist.isPresent());

        merchantController.deleteMerchant(merchantToBeDeletedId);

        Optional<Merchant> merchantThatShouldntExist = merchantController.getMerchant(merchantToBeDeletedId);
        assertTrue(!merchantThatShouldntExist.isPresent());
    }

    @After
    public void tearDown() throws Exception {
        for(String accountId : createdAccounts) {
            merchantController.deleteMerchant(accountId);
        }
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
        // code executed after the last test method
    }
}
