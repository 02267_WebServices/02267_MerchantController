import merchantservice.database.Merchant;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * @Author Jesper Rask Lykke - s144950
 */
public class MerchantTest
{
    String cpr = "020995-1234",
            name = "SomeGuy",
            bankAccountId = "bank account id";
    Merchant merchant;

    @Before
    public void setUp() throws Exception
    {
        Merchant cunstructorTest = new Merchant();
        merchant = new Merchant(cpr, name, bankAccountId);
    }

    @Test
    public void getCpr()
    {
        assertEquals(cpr, merchant.getCpr());
    }

    @Test
    public void getName()
    {
        assertEquals(name, merchant.getName());
    }

    @Test
    public void getBankAccountId()
    {
        assertEquals(bankAccountId, merchant.getBankAccountId());
    }
}
