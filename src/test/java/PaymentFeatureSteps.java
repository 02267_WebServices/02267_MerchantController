// this file defines the "steps" for the PaymentFeatureSteps.feature file

import merchantservice.bankService.BankController;
import merchantservice.bankService.InvalidAccountInfoException;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import io.cucumber.java.After;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.When;
import io.cucumber.java.en.Then;
import merchantservice.restAdapter.DTO.BankTransfer;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * @Author Lukas Nyboe Bek - s153475
 */
public class PaymentFeatureSteps {
    BankService bank = new BankServiceService().getBankServicePort();
    String cusotmerAccount, merchantAccount;
    String description = "This is a test transaction";
    BankController bankController = new BankController(bank);

    @Given("a customer with a bank account")
    public void a_customer_with_a_bank_account() throws BankServiceException_Exception {
        User customer = new User();
        customer.setFirstName("TestPerson1_FirstName");
        customer.setLastName("TestPerson1_LastName");
        customer.setCprNumber("TestPerson1_cpr");

        cusotmerAccount = bank.createAccountWithBalance(customer, new BigDecimal(100));
    }

    @Given("merchant with a bank account")
    public void merchant_with_a_bank_account() throws BankServiceException_Exception {
        User merchant = new User();
        merchant.setFirstName("TestPerson2_FirstName");
        merchant.setLastName("TestPerson2_LastName");
        merchant.setCprNumber("TestPerson2_cpr");

        merchantAccount = bank.createAccountWithBalance(merchant, new BigDecimal(100));
    }

    @When("the customer trasnfers money to the merchant")
    public void the_customer_trasnfers_money_to_the_merchant() throws InvalidAccountInfoException {
        bankController.transferMoney(new BankTransfer(cusotmerAccount, merchantAccount, description, new BigDecimal(30)));
    }

    @Then("the bank account balances should reflect the transfer")
    public void the_bank_account_balances_should_reflect_the_transfer() throws BankServiceException_Exception {
        BigDecimal customerBankMoney = bank.getAccount(cusotmerAccount).getBalance();
        BigDecimal merchantBankMoney = bank.getAccount(merchantAccount).getBalance();

        assertEquals(new BigDecimal(70), customerBankMoney);
        assertEquals(new BigDecimal(130), merchantBankMoney);
    }

    @Then("the transfer description should be in the transaction")
    public void the_transfer_description_should_be_in_the_transaction() throws BankServiceException_Exception {
        String customerDescription = bank.getAccount(cusotmerAccount).getTransactions().get(0).getDescription();
        String merchantDescription = bank.getAccount(merchantAccount).getTransactions().get(0).getDescription();

        assertEquals(customerDescription, description);
        assertEquals(merchantDescription, description);
    }

    @After
    public void after() throws BankServiceException_Exception {
        System.out.println("Deleting "+ cusotmerAccount);
        System.out.println("Deleting "+ merchantAccount);
        bank.retireAccount(cusotmerAccount);
        bank.retireAccount(merchantAccount);
    }
}
