Feature: Payment to the bank
  A transfer is to be made to the bank

  Scenario: A successful transaction goes from customer to merchant
    Given a customer with a bank account
    And  merchant with a bank account
    When the customer trasnfers money to the merchant
    Then the bank account balances should reflect the transfer
    And the transfer description should be in the transaction

