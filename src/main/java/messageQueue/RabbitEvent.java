package messageQueue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Hubert Baumeister
 * Taken from the messageQueueDemo project
 */
public class RabbitEvent {

    private String eventType;
    private Object[] arguments = null;

    public RabbitEvent() {
    }

    ;

    public RabbitEvent(String eventType, Object[] arguments) {
        this.eventType = eventType;
        this.arguments = arguments;
    }

    public RabbitEvent(String type) {
        this.eventType = type;
    }

    public String getEventType() {
        return eventType;
    }

    public Object[] getArguments() {
        return arguments;
    }

    public boolean equals(Object o) {
        if (!this.getClass().equals(o.getClass())) {
            return false;
        }
        RabbitEvent other = (RabbitEvent) o;
        return this.eventType.equals(other.eventType) &&
                (this.getArguments() != null &&
                        Arrays.equals(this.getArguments(), other.getArguments())) ||
                (this.getArguments() == null && other.getArguments() == null);
    }

    public int hashCode() {
        return eventType.hashCode();
    }

    public String toString() {
        List<String> strs = new ArrayList<>();
        if (arguments != null) {
            List<Object> objs = Arrays.asList(arguments);
            strs = objs.stream().map(Object::toString).collect(Collectors.toList());
        }

        return String.format("event(%s,%s)", eventType, String.join(",", strs));
    }
}
