package messageQueue.rabbitmq;

import merchantservice.controller.MerchantController;
import messageQueue.EventReceiver;
import messageQueue.EventSender;
import messageQueue.RabbitEvent;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;


/**
 * @author Lukas Nyboe Bek - s153475
 */
public class RabbitMQAdapter implements EventReceiver {

    private MerchantController merchantController;
    private EventSender eventSender;

    private CompletableFuture<RabbitEvent> validateTokenFuture;
    private CompletableFuture<RabbitEvent> sendTransferMoneyRequest;


    /**
     * @param sender an event sender
     */
    public RabbitMQAdapter(EventSender sender) {
        this.eventSender = sender;
    }

    /**
     * @param merchantController contains the logic for the customer
     */
    public void setController(MerchantController merchantController) {
        this.merchantController = merchantController;
    }

    /**
     * Calls the tokenmanager to validate a token
     *
     * @param tokenId
     * @return either a "false" object or the customers id and bank account id
     * @throws Exception
     */
    public Optional<Object[]> validateToken(String tokenId) throws Exception {
        validateTokenFuture = new CompletableFuture<>();
        Object[] args = {tokenId};
        sendRabbitMessage(MessageQueries.VALIDATE_TOKEN, args);
        RabbitEvent validateTokenEvent = validateTokenFuture.join();

        return Optional.ofNullable(validateTokenEvent.getArguments());
    }

    /**
     * Calls the transaction controller to add a transaction
     *
     * @param amount
     * @param debtorId
     * @param customerId
     * @param tokenId
     * @param description
     * @return true, if transfer is successfull
     * @throws Exception
     */
    public boolean transferMoney(BigDecimal amount, String debtorId, String customerId, String tokenId, String description) throws Exception {
        sendTransferMoneyRequest = new CompletableFuture<>();
        Object[] args = {amount, debtorId, customerId, tokenId, description};
        sendRabbitMessage(MessageQueries.CREATE_TRANSACTION, args);
        RabbitEvent transferMoney = sendTransferMoneyRequest.join();

        if (MessageQueries.TRANSACTION_FAILED_RESPONSE.equals(transferMoney.getEventType())) {
            throw new Exception("Transaction failed from transaction service");
        }

        return MessageQueries.TRANSACTION_COMPLETED_RESPONSE.equals(transferMoney.getEventType());
    }

    /**
     * Helper method used to send rabbit events
     *
     * @param RABBIT_EVENT_MESSAGE the event message
     * @param argsForEvent         the arguments for the event
     */
    private void sendRabbitMessage(String RABBIT_EVENT_MESSAGE, Object[] argsForEvent) throws Exception {
        RabbitEvent rabbitEvent = new RabbitEvent(RABBIT_EVENT_MESSAGE, argsForEvent);
        eventSender.sendEvent(rabbitEvent);
    }


    /**
     * Incoming rabbitMq events.
     */
    @Override
    public void receiveEvent(RabbitEvent event) throws Exception {
        final boolean isEventUserTypeRequest = event.getEventType().equals(MessageQueries.USER_TYPE_REQUEST);
        if (isEventUserTypeRequest) {
            System.out.println("handling event: " + event);
            Object[] args = event.getArguments();
            String userId = (String) args[0];

            final boolean isUserOfTypeMerchant = merchantController.checkIfUserIsMerchant(userId);
            if (!isUserOfTypeMerchant)
                return;

            Object[] responseArgs = {userId, "merchant"};
            RabbitEvent e = new RabbitEvent(MessageQueries.USER_TYPE_IDENTIFIED, responseArgs);
            eventSender.sendEvent(e);
        }
        if (MessageQueries.TOKEN_NOT_VALIDATED.equals(event.getEventType())) {
            validateTokenFuture.complete(event);
        }
        if (MessageQueries.CUSTOMER_BANKACCOUNT_ID_RESPONSE.equals(event.getEventType())) {
            validateTokenFuture.complete(event);
        }
        if (MessageQueries.TRANSACTION_FAILED_RESPONSE.equals(event.getEventType())) {
            sendTransferMoneyRequest.complete(event);
        }
        if (MessageQueries.TRANSACTION_COMPLETED_RESPONSE.equals(event.getEventType())) {
            sendTransferMoneyRequest.complete(event);
        }
    }
}
