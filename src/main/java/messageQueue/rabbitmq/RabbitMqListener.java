package messageQueue.rabbitmq;

import com.google.gson.Gson;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;

import messageQueue.EventReceiver;
import messageQueue.RabbitEvent;

/**
 * @author Hubert Baumeister
 * This is heavily inspired by the messageQueueDemo project
 */
public class RabbitMqListener {

    private static final String EXCHANGE_NAME = "eventsExchange";
    EventReceiver service;

    public RabbitMqListener(EventReceiver service) {
        this.service = service;
    }

    public void listen() throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(System.getenv("RABBIT_MQ_HOSTNAME"));
        Connection connection = factory.newConnection();
        Channel channel = connection.createChannel();
        channel.exchangeDeclare(EXCHANGE_NAME, "topic");
        String queueName = channel.queueDeclare().getQueue();
        channel.queueBind(queueName, EXCHANGE_NAME, "events");

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("[x] receiving " + message);

            RabbitEvent event = new Gson().fromJson(message, RabbitEvent.class);
            try {
                service.receiveEvent(event);
            } catch (Exception e) {
                throw new Error(e);
            }
        };
        channel.basicConsume(queueName, true, deliverCallback, consumerTag -> {
        });
    }
}
