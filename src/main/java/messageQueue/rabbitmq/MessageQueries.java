package messageQueue.rabbitmq;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class MessageQueries {

    public static final String VALIDATE_TOKEN = "UseTokenRequest";
    public static final String TOKEN_NOT_VALIDATED = "TokenNotValidated";

    public static final String USER_TYPE_IDENTIFIED = "UserTypeIdentified";
    public static final String USER_TYPE_REQUEST = "UserTypeRequest";
    public static final String CUSTOMER_BANKACCOUNT_ID_RESPONSE = "BankAccountFromToken";

    public static final String CREATE_TRANSACTION = "CreateTransaction";
    public static final String TRANSACTION_COMPLETED_RESPONSE = "TransactionCreated";
    public static final String TRANSACTION_FAILED_RESPONSE = "TransactionFailed";
}
