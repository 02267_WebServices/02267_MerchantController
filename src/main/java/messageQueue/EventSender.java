package messageQueue;

/**
 * @author Hubert Baumeister
 * Taken from the messageQueueDemo project
 */
public interface EventSender {

    void sendEvent(RabbitEvent event) throws Exception;

}
