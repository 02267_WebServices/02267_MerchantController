package messageQueue;

/**
 * @author Hubert Baumeister
 * Taken from the messageQueueDemo project
 */
public interface EventReceiver {
    void receiveEvent(RabbitEvent event) throws Exception;
}
