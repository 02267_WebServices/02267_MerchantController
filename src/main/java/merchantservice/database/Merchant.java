package merchantservice.database;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class Merchant {

    String cpr, name, bankAccountId;

    public String getCpr() {
        return cpr;
    }

    public String getName() {
        return name;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }

    public Merchant(String cpr, String name, String bankAccountId) {
        this.cpr = cpr;
        this.name = name;
        this.bankAccountId = bankAccountId;
    }

    public Merchant() {
    }

}
