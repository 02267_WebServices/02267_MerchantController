package merchantservice.database;

import java.util.Optional;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public interface IDAO<T> {

    /**
     * Adds a merchant to the merchantservice.database
     * @param t A merchant object
     * @return the string of the created merchant
     */
    String addMerchant(T t);

    /**
     * Deletes a merchant from the merchantservice.database
     * @param merchantId of the merchant to be deleted
     * @return true if deleted succesfully
     */
    boolean delete(String merchantId);

    /**
     * Updates a merchant in the merchantservice.database
     * @param id of the merchant to be updated
     * @param t Merchant object to update with
     * @return the updated merchant
     */
    Merchant updateMerchant(String id, T t);

    /**
     * Gets a merchant from the merchantservice.database
     * @param id of the merchant to get
     * @return optional containing the merchant
     */
    Optional<Merchant> getMerchant(String id);
}
