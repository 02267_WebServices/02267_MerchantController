package merchantservice.database;

import java.util.HashMap;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class MerchantDAO implements IDAO<Merchant> {

    // The merchnats are stored in a HashMap
    private HashMap<String, Merchant> merchants = new HashMap<>();

    public MerchantDAO() {
        // Merchants to populate the merchantservice.database -
        merchants.put("1", new Merchant("1234567890", "Awesome-Augusts Amazing Apfelstrudels", "123"));
        merchants.put("2", new Merchant("9876543210", "Hellige-Haakons Henrivende Hindbærsnitter", "789"));
        merchants.put("3", new Merchant("1357924680", "Super-Sebbys Sexede Stroopwaffles", "456"));
        merchants.put("4", new Merchant("2468013579", "Talenfulde-Troels Tiptop Tebirkes", "654"));
        merchants.put("5", new Merchant("0897645312", "Jesuitiske Jespers Jyske Jødekager", "150"));
    }

    @Override
    public String addMerchant(Merchant m) {
        String uniqueId = UUID.randomUUID().toString();
        merchants.put(uniqueId, m);
        return uniqueId;
    }

    @Override
    public boolean delete(String merchantId) {
        Optional<Merchant> merchant = Optional.ofNullable(merchants.remove(merchantId));
        return merchant.isPresent();
    }

    @Override
    public Merchant updateMerchant(String id, Merchant m) {
        return merchants.replace(id, m);
    }

    @Override
    public Optional<Merchant> getMerchant(String id) {
        return Optional.ofNullable(merchants.get(id));
    }
}
