package merchantservice.restAdapter;

import merchantservice.controller.MerchantController;
import merchantservice.controller.SetupFactory;
import merchantservice.database.Merchant;
import merchantservice.restAdapter.DTO.TransferRequest;
import merchantservice.restAdapter.DTO.UpdateMerchant;

import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.Optional;

/**
 * @author Lukas Nyboe Bek - s153475
 */
@Path("/merchant")
public class MerchantService {

    MerchantController merchantController = new SetupFactory().initOrGetMerchantController();

    @GET
    @Path("/test")
    @Produces("text/plain")
    public Response doGet() {
        return Response.ok("Connected to Thorntail!").build();
    }

    @POST
    @Path("/transaction")
    @Consumes("application/json")
    public Response transferPayment(TransferRequest tr) {
        try {
            merchantController.initiateTransfer(tr);
        } catch (Exception e) {
            Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
        return Response.ok().build();

    }

    @DELETE
    @Path("/merchant")
    public Response deleteMerchant(@QueryParam("id") String merchantId) {
        boolean succeeded = merchantController.deleteMerchant(merchantId);
        if (succeeded) {
            return Response.status(Response.Status.NO_CONTENT).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }

    @POST
    @Path("/merchant")
    @Consumes("application/json")
    public Response createMerchant(Merchant merchant) {
        String id = merchantController.addMerchant(merchant);
        return Response.status(Response.Status.CREATED).entity(id).build();
    }
}
