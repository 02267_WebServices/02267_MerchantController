package merchantservice.restAdapter.DTO;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class UpdateMerchant {

    String merchantId;
    String cpr;
    String name;
    String bankAccountId;

    public UpdateMerchant(){
        // Needed for constructing the object from REST calls
    }

    public UpdateMerchant(String merchantId, String cpr, String name, String bankAccountId) {
        this.merchantId = merchantId;
        this.cpr = cpr;
        this.name = name;
        this.bankAccountId = bankAccountId;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getCpr() {
        return cpr;
    }

    public String getName() {
        return name;
    }

    public String getBankAccountId() {
        return bankAccountId;
    }
}
