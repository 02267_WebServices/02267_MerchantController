package merchantservice.restAdapter.DTO;

import java.math.BigDecimal;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class TransferRequest {

    public String debtorId;
    public String description;
    public String tokenId;
    public BigDecimal amount;


    public TransferRequest(){
        // Needed for constructing the object from REST calls
    }
}
