package merchantservice.restAdapter.DTO;

import java.math.BigDecimal;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class BankTransfer {

    public String debtorBankAccountId;
    public String customerBankAccountId;
    public String description;
    public BigDecimal amount;

    public BankTransfer(String debitorBankAccountId, String customerBankAccountId, String description, BigDecimal amount) {
        this.debtorBankAccountId = debitorBankAccountId;
        this.customerBankAccountId = customerBankAccountId;
        this.description = description;
        this.amount = amount;
    }


    public BankTransfer(){
        // Needed for constructing the object in the REST calls
    }
}
