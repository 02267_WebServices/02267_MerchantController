package merchantservice.bankService;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class InvalidAccountInfoException extends Exception {
    public InvalidAccountInfoException(String errorMessage, Throwable err) {
        super(errorMessage, err);
    }
}
