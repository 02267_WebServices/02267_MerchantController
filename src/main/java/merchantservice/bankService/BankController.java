package merchantservice.bankService;

import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import merchantservice.restAdapter.DTO.BankTransfer;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class BankController {

    BankService bank;

    public BankController(BankService bank) {
        this.bank = bank;
    }

    /**
     * The request to send a transfer to the bank
     * @param request containing the transfer info
     * @throws InvalidAccountInfoException if the account doesn't exist
     */
    public void transferMoney(BankTransfer request) throws InvalidAccountInfoException {
        try {
            bank.transferMoneyFromTo(request.debtorBankAccountId, request.customerBankAccountId, request.amount, request.description);
        } catch (BankServiceException_Exception e) {
            throw new InvalidAccountInfoException("Invalid account info", e);
        }
    }
}
