package merchantservice.controller;

import merchantservice.bankService.BankController;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceService;
import messageQueue.EventSender;
import messageQueue.rabbitmq.RabbitMQAdapter;
import messageQueue.rabbitmq.RabbitMqListener;
import messageQueue.rabbitmq.RabbitMqSender;

/**
 * @author Hubert Baumeister
 * This is heavily inspired by the messageQueueDemo factory
 */
public class SetupFactory {

    static MerchantController merchantController = null;

    /**
     * Initializes a merchantcontroller or gets it if it already exists
     * @return merchantcontroller
     */
    public MerchantController initOrGetMerchantController(){
        if(merchantController != null) {
            return merchantController;
        }

        BankService bank = new BankServiceService().getBankServicePort();
        BankController bankController = new BankController(bank);
        EventSender b = new RabbitMqSender();
        RabbitMQAdapter rabbitMQAdapter = new RabbitMQAdapter(b);
        merchantController = new MerchantController(bankController, rabbitMQAdapter);

        rabbitMQAdapter.setController(merchantController);
        RabbitMqListener listener = new RabbitMqListener(rabbitMQAdapter);
        try {
            listener.listen();
        } catch (Exception e) {
            throw  new Error(e);
        }

        return merchantController;

    }
}
