package merchantservice.controller;

import java.math.BigDecimal;
import java.util.Optional;

import merchantservice.bankService.BankController;
import merchantservice.database.IDAO;
import merchantservice.database.Merchant;
import merchantservice.database.MerchantDAO;
import messageQueue.rabbitmq.RabbitMQAdapter;
import merchantservice.restAdapter.DTO.BankTransfer;
import merchantservice.restAdapter.DTO.TransferRequest;
import merchantservice.restAdapter.DTO.UpdateMerchant;

/**
 * @author Lukas Nyboe Bek - s153475
 */
public class MerchantController {

    private BankController bankController;
    private RabbitMQAdapter rabbitMQAdapter;
    private IDAO<Merchant> merchantDAO;

    public MerchantController(BankController bankController, RabbitMQAdapter rabbitMQAdapter) {
        this.bankController = bankController;
        this.rabbitMQAdapter = rabbitMQAdapter;
        merchantDAO = new MerchantDAO();
    }

    /**
     * The initial method for starting the transfer. Firstly it checks whether the token is valid and the either completes or stops the transfer
     *
     * @param request containing the information needed for the transfer
     * @return true if the transfer succeeds
     * @throws Exception if the token is not valid or no answer was recieved from the validateToken
     */
    public void initiateTransfer(TransferRequest request) throws Exception {
        Optional<Object[]> answer = rabbitMQAdapter.validateToken(request.tokenId);
        try {
            Boolean.parseBoolean(String.valueOf(answer.get()[0]));
            throw new Exception("Invalid token");
        } catch (Exception e) {
            String customerId = answer.get()[0].toString();
            String customerBankAccountId = answer.get()[1].toString();
            transferMoney(request, customerId, customerBankAccountId);
        }
    }

    public boolean checkIfUserIsMerchant(String id) {
        return !getMerchant(id).equals(Optional.empty());
    }


    // DATABASE SCOPE

    /**
     * Used to get a merchant from the merchantservice.database
     *
     * @param id merchant id
     * @return an optional containing the merchant
     */
    public Optional<Merchant> getMerchant(String id) {
        return (merchantDAO.getMerchant(id));
    }

    /**
     * Adds a merchant to the merchantservice.database
     *
     * @param merchant to be added
     * @return the merchant id created in the process
     */
    public String addMerchant(Merchant merchant) {
        return merchantDAO.addMerchant(merchant);
    }

    /**
     * Delete a merchant
     *
     * @param merchantId of the merchant to be deleted
     * @return true if a merchant is deleted
     */
    public boolean deleteMerchant(String merchantId) {
        return merchantDAO.delete(merchantId);
    }

    /**
     * Used to update a merchant
     *
     * @param updatedMerchant containing information used to update the merchant based on the id of updatedMerchant
     * @return the updated merchant
     */
    public Merchant updateMerchant(UpdateMerchant updatedMerchant) {
        Merchant merchant = new Merchant(updatedMerchant.getCpr(), updatedMerchant.getName(), updatedMerchant.getBankAccountId());
        return merchantDAO.updateMerchant(updatedMerchant.getMerchantId(), merchant);
    }
    // END DATABASE SCOPE


    // PRIVATE HELPER METHODS

    /**
     * Registers the transfer to the DTU pay app, and on success, sends the transaction to the bank
     *
     * @param request               containing the information about the transfer
     * @param customerId            id of the customer
     * @param customerBankAccountId id of the customers bank account
     * @throws Exception
     */
    private void transferMoney(TransferRequest request, String customerId, String customerBankAccountId) throws Exception {
        boolean transferComplete = rabbitMQAdapter.transferMoney(request.amount, request.debtorId, customerId, request.tokenId, request.description);

        if (!transferComplete) {
            throw new Exception("Internal transfer error to transaction controller");
        }
        Optional<Merchant> merchant = merchantDAO.getMerchant(request.debtorId);
        if (!merchant.isPresent()) {
            throw new Exception("Merchant doesn't exist");
        }

        transferToBank(request, customerBankAccountId);
    }

    /**
     * Registers the transfer to the bank
     *
     * @param request               containing the information about the transfer
     * @param customerBankAccountId the bank account of the customer
     * @throws Exception
     */
    private void transferToBank(TransferRequest request, String customerBankAccountId) throws Exception {
        BankTransfer bankTransfer = null;
        Optional<Merchant> merchant = merchantDAO.getMerchant(request.debtorId);
        if (!merchant.isPresent()) {
            throw new Exception("Merchant doesn't exist");
        }

        if (BigDecimal.ZERO.compareTo(request.amount) < 0) {
            bankTransfer = new BankTransfer(
                    merchant.get().getBankAccountId(),
                    customerBankAccountId,
                    request.description,
                    request.amount);
        } else if (BigDecimal.ZERO.compareTo(request.amount) > 0) {
            bankTransfer = new BankTransfer(
                    customerBankAccountId,
                    merchant.get().getBankAccountId(),
                    request.description,
                    request.amount.negate());
        }
        bankController.transferMoney(bankTransfer);
    }

}
