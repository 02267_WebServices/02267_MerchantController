# Troels - s120052
FROM adoptopenjdk:8-jre-hotspot
WORKDIR /usr/src
COPY target/02267_MerchantController-1.0-SNAPSHOT-thorntail.jar /usr/src
CMD java -Xmx64m \
    -Djava.net.preferIPv4Stack=true \
    -Djava.net.preferIPv4Addresses=true \
    -jar 02267_MerchantController-1.0-SNAPSHOT-thorntail.jar